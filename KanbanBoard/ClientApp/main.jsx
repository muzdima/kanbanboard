﻿import "./css/site.css";
import "bootstrap";
import React from "react";
import ReactDom from "react-dom";
import { AppContainer } from "react-hot-loader";
import Board from "./components/Board";

ReactDom.render(
	<AppContainer>
		<Board />
	</AppContainer>,
	document.getElementById("react-app")
);
