﻿import React from "react";
import { Modal } from "react-bootstrap";

export default class InputDialog extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			title: props.title,
			description: props.description
		};
	}
	updateTitle(event) {
		this.setState({ title: event.target.value, description: this.state.description });
	}
	updateDescription(event) {
		this.setState({ title: this.state.title, description: event.target.value });
	}
	render() {
		return <Modal show={true} onHide={this.props.onCancel}>
			<Modal.Header closeButton>
				<Modal.Title>{this.props.header}</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<div className="form-group">
					<label htmlFor="inputTitle">Title</label>
					<input type="text" className="form-control" id="inputTitle" value={this.state.title} onChange={event => this.updateTitle(event)} />
				</div>
				<div className="form-group">
					<label htmlFor="inputDescription">Description</label>
					<input type="text" className="form-control" id="inputDescription" value={this.state.description} onChange={event => this.updateDescription(event)} />
				</div>
				<button type="button" className="btn btn-primary" onClick={() => this.props.onSubmit(this.state.title, this.state.description)}>Submit</button>
			</Modal.Body>
		</Modal>;
	}
};