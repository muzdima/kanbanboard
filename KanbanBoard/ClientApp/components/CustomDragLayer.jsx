﻿import React from 'react';
import { DragLayer } from 'react-dnd';
import Card from './Card';

function collect(monitor) {
	return {
		item: monitor.getItem(),
		itemType: monitor.getItemType(),
		initialOffset: monitor.getInitialSourceClientOffset(),
		currentOffset: monitor.getSourceClientOffset(),
		isDragging: monitor.isDragging()
	}
};

class CustomDragLayer extends React.PureComponent {
	getStyle() {
		if (!this.props.initialOffset || !this.props.currentOffset) return { display: "none" };
		const transform = `translate(${this.props.currentOffset.x}px, ${this.props.currentOffset.y}px)`;
		return {
			transform,
			WebkitTransform: transform
		}
	}
	render() {
		if (!this.props.isDragging) return null;
		return <div style={this.getStyle()} className="drag-layer">
			<Card
				title={this.props.cards[this.props.item.id].title}
				description={this.props.cards[this.props.item.id].description}
				onEdit={null}
				onDelete={null} />
		</div>;
	}
};

export default DragLayer(collect)(CustomDragLayer);