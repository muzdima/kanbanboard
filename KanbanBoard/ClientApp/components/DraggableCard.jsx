﻿import React from "react";
import { getEmptyImage } from "react-dnd-html5-backend";
import { DragSource, DropTarget } from "react-dnd";
import { findDOMNode } from 'react-dom'
import Card from "./Card";

const cardSource = {
	beginDrag(props) {
		props.onDragBegin(props.id);
		return { id: props.id };
	},
	endDrag(props) {
		props.onDragEnd();
	}
};

const cardTarget = {
	hover(props, monitor, component) {
		props.onPush(monitor.getItem().id, props.id, findDOMNode(component).getBoundingClientRect(), monitor.getClientOffset());
	},
	drop(props, monitor) {
		props.onMove(monitor.getItem().id);
	}
};

function collectSource(connect) {
	return {
		connectDragSource: connect.dragSource(),
		connectDragPreview: connect.dragPreview()
	}
};

function collectTarget(connect) {
	return {
		connectDropTarget: connect.dropTarget()
	}
};

class DraggableCard extends React.PureComponent {
	render() {
		return this.props.connectDragSource(this.props.connectDropTarget(
			<div style={{ opacity: this.props.isHide ? 0 : 1 }}><Card
				title={this.props.title}
				description={this.props.description}
				onEdit={this.props.onEdit}
				onDelete={this.props.onDelete}
			/></div>
		));
	}
	componentDidMount() {
		this.props.connectDragPreview(getEmptyImage(), { captureDraggingState: true });
	}
};

export default DragSource("card", cardSource, collectSource)(DropTarget("card", cardTarget, collectTarget)(DraggableCard));