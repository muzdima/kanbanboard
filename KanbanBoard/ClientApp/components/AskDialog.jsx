﻿import React from "react";
import { Modal } from "react-bootstrap";

export default class AskDialog extends React.PureComponent {
	render() {
		return <Modal show={true} onHide={this.props.onCancel}>
			<Modal.Header closeButton>
				<Modal.Title>{this.props.header}</Modal.Title>
			</Modal.Header>
			<Modal.Body>
				<p>{this.props.text}</p>
				<button type="button" className="btn btn-primary" onClick={this.props.onSubmit}>OK</button>
				<button type="button" className="btn btn-default" onClick={this.props.onCancel}>Cancel</button>
			</Modal.Body>
		</Modal>;
	}
};