﻿import React from "react";

export default class Card extends React.PureComponent {
	render() {
		return <div className="card">
			<div className="control">
				{this.props.onEdit != null && <button type="button" className="btn btn-default btn-xs" onClick={this.props.onEdit}><span className="glyphicon glyphicon-pencil" aria-hidden="true"></span></button>}
				{this.props.onDelete != null && <button type="button" className="btn btn-default btn-xs" onClick={this.props.onDelete}><span className="glyphicon glyphicon-remove" aria-hidden="true"></span></button>}
			</div>
			<div className="card-content">
				<h4><b>{this.props.title}</b></h4>
				<p>{this.props.description}</p>
			</div>
		</div>;
	}
};