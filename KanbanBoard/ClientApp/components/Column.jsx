﻿import React from "react";
import { DropTarget } from "react-dnd";
import DraggableCard from "./DraggableCard";

const columnTarget = {
	hover(props, monitor) {
		if (monitor.isOver({ shallow: true }))
			props.onPushColumn(monitor.getItem().id, props.index);
	},
	drop(props, monitor) {
		if (!monitor.didDrop())
			props.onMove(monitor.getItem().id);
	}
};

function collectTarget(connect) {
	return {
		connectDropTarget: connect.dropTarget()
	}
};

class Column extends React.PureComponent {
	render() {
		return this.props.connectDropTarget(<div className="column">
			<h2 className="text-center">{this.props.title}</h2>
			<div className="column-content">
				{this.props.cards.map((card) =>
					<DraggableCard
						key={card.id.toString()}
						id={card.id}
						title={card.title}
						description={card.description}
						isHide={this.props.dragId===card.id}
						onEdit={() => this.props.onEdit(card)}
						onDelete={() => this.props.onDelete(card)}
						onPush={this.props.onPush}
						onMove={this.props.onMove}
						onDragBegin={this.props.onDragBegin}
						onDragEnd={this.props.onDragEnd}
					/>
				)}
			</div>
		</div>);
	}
};

export default DropTarget("card", columnTarget, collectTarget)(Column);