﻿import React from "react";
import { DragDropContext } from "react-dnd";
import HTML5Backend from "react-dnd-html5-backend";

import Column from "./Column";
import InputDialog from "./InputDialog";
import AskDialog from "./AskDialog";
import CustomDragLayer from "./CustomDragLayer";

var actions = Object.freeze({ input: 1, delete: 2 });

class Board extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = { columns: [], action: null, actionCard: null, cards: {}, cardsToColumnIndex: {}, dragCardId: null, isLoading: false };
	}
	add() {
		this.setState({ columns: this.state.columns, action: actions.input, actionCard: null, cards: this.state.cards, cardsToColumnIndex: this.state.cardsToColumnIndex, dragCardId: this.state.dragCardId, isLoading: this.state.isLoading });
	}
	editCard(card) {
		this.setState({ columns: this.state.columns, action: actions.input, actionCard: card, cards: this.state.cards, cardsToColumnIndex: this.state.cardsToColumnIndex, dragCardId: this.state.dragCardId, isLoading: this.state.isLoading });
	}
	deleteCard(card) {
		this.setState({ columns: this.state.columns, action: actions.delete, actionCard: card, cards: this.state.cards, cardsToColumnIndex: this.state.cardsToColumnIndex, dragCardId: this.state.dragCardId, isLoading: this.state.isLoading });
	}
	cancel() {
		this.setState({ columns: this.state.columns, action: null, actionCard: null, cards: this.state.cards, cardsToColumnIndex: this.state.cardsToColumnIndex, dragCardId: this.state.dragCardId, isLoading: this.state.isLoading });
	}
	dragBegin(dragId) {
		this.setState({ columns: this.state.columns, action: this.state.action, actionCard: this.state.actionCard, cards: this.state.cards, cardsToColumnIndex: this.state.cardsToColumnIndex, dragCardId: dragId, isLoading: this.state.isLoading });
	}
	dragEnd() {
		this.setState({ columns: this.state.columns, action: this.state.action, actionCard: this.state.actionCard, cards: this.state.cards, cardsToColumnIndex: this.state.cardsToColumnIndex, dragCardId: null, isLoading: this.state.isLoading });
	}
	push(dragId, hoverId, hoverBoundingRect, clientOffset) {
		if (dragId === hoverId) return;

		const dragColumnIndex = this.state.cardsToColumnIndex[dragId];
		const hoverColumnIndex = this.state.cardsToColumnIndex[hoverId];
		const dragIndex = this.state.columns[dragColumnIndex].cards.findIndex(card => card.id === dragId);
		const hoverIndex = this.state.columns[hoverColumnIndex].cards.findIndex(card => card.id === hoverId);

		if (this.dragLayerWrap && this.dragLayerWrap.firstChild) {
			const dragBoundingRect = this.dragLayerWrap.firstChild.getBoundingClientRect();
			if (dragColumnIndex === hoverColumnIndex && dragIndex < hoverIndex) {
				if (hoverBoundingRect.bottom - clientOffset.y > dragBoundingRect.height) return;
			} else {
				if (clientOffset.y - hoverBoundingRect.top > dragBoundingRect.height) return;
			}
		}

		const columnsNew = this.state.columns.slice();
		let cardsToColumnIndexNew = this.state.cardsToColumnIndex;
		columnsNew[dragColumnIndex] = Object.assign({}, this.state.columns[dragColumnIndex]);
		columnsNew[dragColumnIndex].cards = this.state.columns[dragColumnIndex].cards.slice();
		if (dragColumnIndex !== hoverColumnIndex) {
			columnsNew[hoverColumnIndex] = Object.assign({}, this.state.columns[hoverColumnIndex]);
			columnsNew[hoverColumnIndex].cards = this.state.columns[hoverColumnIndex].cards.slice();
			cardsToColumnIndexNew = Object.assign({}, this.state.cardsToColumnIndex);
			cardsToColumnIndexNew[dragId] = hoverColumnIndex;
		}

		columnsNew[hoverColumnIndex].cards.splice(hoverIndex + (dragColumnIndex === hoverColumnIndex && dragIndex < hoverIndex ? 1 : 0), 0, this.state.columns[dragColumnIndex].cards[dragIndex]);
		columnsNew[dragColumnIndex].cards.splice(dragIndex + (dragColumnIndex === hoverColumnIndex && dragIndex > hoverIndex ? 1 : 0), 1);

		this.setState({ columns: columnsNew, action: this.state.action, actionCard: this.state.actionCard, cards: this.state.cards, cardsToColumnIndex: cardsToColumnIndexNew, dragCardId: this.state.dragCardId, isLoading: this.state.isLoading });
	}
	pushColumn(dragId, columnIndex) {
		const dragColumnIndex = this.state.cardsToColumnIndex[dragId];
		if (columnIndex === dragColumnIndex) return;
		const dragIndex = this.state.columns[dragColumnIndex].cards.findIndex(card => card.id === dragId);

		const cardsToColumnIndexNew = Object.assign({}, this.state.cardsToColumnIndex);
		cardsToColumnIndexNew[dragId] = columnIndex;

		const columnsNew = this.state.columns.slice();
		columnsNew[columnIndex] = Object.assign({}, this.state.columns[columnIndex]);
		columnsNew[dragColumnIndex] = Object.assign({}, this.state.columns[dragColumnIndex]);
		columnsNew[columnIndex].cards = this.state.columns[columnIndex].cards.slice();
		columnsNew[dragColumnIndex].cards = this.state.columns[dragColumnIndex].cards.slice();

		columnsNew[columnIndex].cards.push(this.state.columns[dragColumnIndex].cards[dragIndex]);
		columnsNew[dragColumnIndex].cards.splice(dragIndex, 1);

		this.setState({ columns: columnsNew, action: this.state.action, actionCard: this.state.actionCard, cards: this.state.cards, cardsToColumnIndex: cardsToColumnIndexNew, dragCardId: this.state.dragCardId, isLoading: this.state.isLoading });
	}
	fetch() {
		fetch("api/GetCards")
			.then(response => response.json())
			.then(data => this.setState({
				columns: data,
				action: null,
				currentCard: null,
				cards: data
					.map(column => column.cards)
					.reduce((accumulator, cards) => accumulator.concat(cards), [])
					.reduce(function (map, card) {
						map[card.id] = card;
						return map;
					}, {}),
				cardsToColumnIndex: data
					.map((column, columnIndex) => column.cards
						.reduce(function (map, card) {
							map[card.id] = columnIndex;
							return map;
						}, {}))
					.reduce((result, map) => Object.assign(result, map), {}),
				dragCardId: this.state.dragCardId,
				isLoading: false
			}));
	}
	componentDidMount() {
		this.setState({ columns: this.state.columns, action: this.state.action, actionCard: this.state.actionCard, cards: this.state.cards, cardsToColumnIndex: this.state.cardsToColumnIndex, dragCardId: this.state.dragCardId, isLoading: true });
		this.fetch();
	}
	input(title, description) {
		fetch(this.state.actionCard == null ? "api/AddCard" : "api/UpdateCard",
			{
				method: "post",
				headers: { 'Content-Type': "application/json" },
				body: JSON.stringify({ title: title, description: description, id: (this.state.actionCard == null ? 0 : this.state.actionCard.id) })
			})
			.then(() => this.fetch());
	}
	delete() {
		fetch("api/DeleteCard",
			{
				method: "post",
				headers: { 'Content-Type': "application/json" },
				body: this.state.actionCard.id.toString()
			})
			.then(() => this.fetch());
	}
	move(dragId) {
		fetch("api/MoveCard",
			{
				method: "post",
				headers: { 'Content-Type': "application/json" },
				body: JSON.stringify({ id: dragId, columnIndex: this.state.cardsToColumnIndex[dragId], index: this.state.columns[this.state.cardsToColumnIndex[dragId]].cards.findIndex(card => card.id === dragId) })
			})
			.then(() => this.fetch());
	}
	render() {
		if (this.state.isLoading) return <img src="loading.svg" alt="Loading..." className="loading" />;
		return <div className="container content">
			<span ref={node => { this.dragLayerWrap = node; }}><CustomDragLayer cards={this.state.cards} /></span>
			<button type="button" className="btn btn-primary create-button" onClick={() => this.add()}>Create</button>
			<div className="row content">
				{this.state.columns.map((column, index) =>
					<div key={index.toString()} className="col-xs-4 content">
						<Column
							index={index}
							title={column.title}
							cards={column.cards}
							dragId={this.state.dragCardId}
							onEdit={card => this.editCard(card)}
							onDelete={card => this.deleteCard(card)}
							onPush={(dragId, hoverId, hoverBoundingRect, clientOffset) => this.push(dragId, hoverId, hoverBoundingRect, clientOffset)}
							onPushColumn={(dragId, columnIndex) => this.pushColumn(dragId, columnIndex)}
							onMove={dragId => this.move(dragId)}
							onDragBegin={dragId => this.dragBegin(dragId)}
							onDragEnd={() => this.dragEnd()}
						/>
					</div>
				)}
			</div>
			{this.state.action === actions.input &&
				<InputDialog
					onSubmit={(title, description) => this.input(title, description)}
					onCancel={() => this.cancel()}
					title={this.state.actionCard == null ? "" : this.state.actionCard.title}
					description={this.state.actionCard == null ? "" : this.state.actionCard.description}
					header={this.state.actionCard == null ? "Create card" : "Edit card"} />}
			{this.state.action === actions.delete &&
				<AskDialog
					onSubmit={() => this.delete()}
					onCancel={() => this.cancel()}
					header="Delete card"
					text="Are you sure you want to delete?" />}
		</div>;
	}
};

export default DragDropContext(HTML5Backend)(Board);