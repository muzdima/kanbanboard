using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using KanbanBoard.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace KanbanBoard.Controllers
{
	public class HomeController : Controller
	{
		private readonly BoardContext _db;

		public HomeController(BoardContext db)
		{
			_db = db;
		}


		public IActionResult Index()
		{
			return View();
		}


		public IActionResult Error()
		{
			ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
			return View();
		}


		[HttpGet("api/[action]")]
		public IEnumerable<Column> GetCards()
		{
			return Enum.GetValues(typeof(CardState)).OfType<CardState>().Select(state => new Column
			{
				Title = state.GetDescription(),
				Cards = _db.Cards.Where(c => c.State == state).OrderBy(c => c.Order).ToArray()
			});
		}


		[HttpPost("api/[action]")]
		public void AddCard([FromBody, Bind("Title", "Description")]Card card)
		{
			card.State = Enum.GetValues(typeof(CardState)).Cast<CardState>().FirstOrDefault();
			card.Order = _db.Cards.Where(c => c.State == card.State).Max(c => (int?)c.Order + 1) ?? 0;
			_db.Cards.Add(card);
			_db.SaveChanges();
		}


		[HttpPost("api/[action]")]
		public void UpdateCard([FromBody, Bind("Id", "Title", "Description")]Card card)
		{
			var found = _db.Cards.Find(card.Id);
			if (found == null) return;
			found.Title = card.Title;
			found.Description = card.Description;
			_db.SaveChanges();
		}


		[HttpPost("api/[action]")]
		public void DeleteCard([FromBody]int id)
		{
			var card = _db.Cards.Find(id);
			if (card == null) return;
			_db.Cards.Remove(card);
			_db.SaveChanges();
			_db.Database.ExecuteSqlCommand("UPDATE [Cards] SET [Order]=[Order]-1 WHERE [State]={0} AND [Order]>{1}", card.State, card.Order);
		}


		public class MoveParams
		{
			public int Id { get; set; }
			public int ColumnIndex { get; set; }
			public int Index { get; set; }
		}


		[HttpPost("api/[action]")]
		public void MoveCard([FromBody]MoveParams move)
		{
			if (move.Index < 0) return;
			var card = _db.Cards.Find(move.Id);
			if (card == null) return;
			var states = Enum.GetValues(typeof(CardState)).Cast<CardState>().ToArray();
			if (move.ColumnIndex < 0 || move.ColumnIndex >= states.Length) return;
			var state = states[move.ColumnIndex];
			_db.Database.ExecuteSqlCommand("UPDATE [Cards] SET [Order]=[Order]-1 WHERE [State]={0} AND [Order]>{1}", card.State, card.Order);
			var order = _db.Cards.Where(c => c.State == state && c.Id != card.Id).OrderBy(c => c.Order).Skip(move.Index).FirstOrDefault()?.Order
				?? _db.Cards.Where(c => c.State == state).Max(c => (int?)c.Order + 1)
				?? 0;
			_db.Database.ExecuteSqlCommand("UPDATE [Cards] SET [Order]=[Order]+1 WHERE [State]={0} AND [Order]>={1}", state, order);
			card.Order = order;
			card.State = state;
			_db.Update(card);
			_db.SaveChanges();
		}

	}
}
