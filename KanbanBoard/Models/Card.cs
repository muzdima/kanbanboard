﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Runtime.Serialization;


namespace KanbanBoard.Models
{
	public enum CardState
	{
		ToDo,

		[Description("In Progress")]
		InProgress,

		Done
	}


	public static class CardStateExtension
	{
		public static string GetDescription(this CardState value)
		{
			var name = Enum.GetName(typeof(CardState), value);
			if (name == null) return null;
			var field = typeof(CardState).GetField(name);
			return field == null ? name : (Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute)?.Description ?? name;
		}
	}


	[DataContract]
	public class Card
	{
		[Key, DatabaseGenerated(DatabaseGeneratedOption.Identity), DataMember]
		public int Id { get; set; }

		[Required, DataMember]
		public string Title { get; set; }

		[Required, DataMember]
		public string Description { get; set; }

		[Required]
		public CardState State { get; set; }

		[Required]
		public int Order { get; set; }
	}
}
