﻿using Microsoft.EntityFrameworkCore;

namespace KanbanBoard.Models
{
	public class BoardContext : DbContext
	{
		public BoardContext(DbContextOptions options): base(options)
		{
		}

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Card>().HasIndex("State", "Order");
		}

		public DbSet<Card> Cards { get; set; }
	}
}
