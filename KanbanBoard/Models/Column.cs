﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KanbanBoard.Models
{
	[DataContract]
	public class Column
	{
		[DataMember]
		public string Title { get; set; }

		[DataMember]
		public IEnumerable<Card> Cards { get; set; }
	}
}
