const path = require("path");
const webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CheckerPlugin = require("awesome-typescript-loader").CheckerPlugin;
const CompressionPlugin = require("compression-webpack-plugin");
const BrotliPlugin = require("brotli-webpack-plugin");
const bundleOutputDir = "./wwwroot/";

module.exports = (env) => {
	const isDevBuild = !(env && env.prod);
	return [{
		stats: { modules: false },
		entry: { 'main': "./ClientApp/main.jsx" },
		resolve: {
			extensions: [".js", ".jsx", ".ts", ".tsx"]
		},
		output: {
			path: path.join(__dirname, bundleOutputDir),
			filename: "[name].js",
			publicPath: "/"
		},
		module: {
			rules: [
				{ test: /\.jsx?$/, loader: "babel-loader", exclude: /node_modules/, query: { presets: ["es2015", "react"] } },
				{ test: /\.tsx?$/, include: /ClientApp/, use: "awesome-typescript-loader?silent=true" },
				{ test: /\.css$/, use: isDevBuild ? ["style-loader", "css-loader"] : ExtractTextPlugin.extract({ use: "css-loader?minimize" }) },
				{ test: /\.(png|jpg|jpeg|gif|svg)$/, use: "url-loader?limit=25000" }
			]
		},
		plugins: [
			new CheckerPlugin(),
			new webpack.DllReferencePlugin({
				context: __dirname,
				manifest: require("./wwwroot/vendor-manifest.json")
			})
		].concat(isDevBuild ? [
			// Plugins that apply in development builds only
			new webpack.SourceMapDevToolPlugin({
				filename: "[file].map", // Remove this line if you prefer inline source maps
				moduleFilenameTemplate: path.relative(bundleOutputDir, "[resourcePath]") // Point sourcemap entries to the original file locations on disk
			})
		] : [
				// Plugins that apply in production builds only
				new webpack.optimize.UglifyJsPlugin(),
				new ExtractTextPlugin("site.css")
			]).concat([
			new CompressionPlugin({
				asset: "[path].gz[query]",
				algorithm: "gzip",
				test: /\.js$|\.css$/
				}),
			new BrotliPlugin({
				asset: "[path].br[query]",
				test: /\.js$|\.css$|\.html$/
			})])
	}];
};